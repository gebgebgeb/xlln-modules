# XLiveLessNess (XLLN)
Games for Windows LiveLessNess. A complete Games For Windows - LIVE<sup>TM</sup> (GFWL) rewrite.

# XLLN-Modules
The purpose of an XLiveLessNess-Module is to enhance some aspect of one or many GFWL Games/Titles or XLiveLessNess itself.
For example one XLLN-Module may only enhance a single Title in ways such as additional configuration, windowed or borderless modes or even bug fixes to the Title itself. Another XLLN-Module could instead focus on broad functionalities such as online play (XLLN-Online), embedding/overlaying the XLiveLessNess interface on top of Title windows, or features across many Titles for example.

It is expected that XLLN-Modules that target specific Titles are named after the Title itself. If the codebase is inherited/migrated from another existing project then the name should be XLLN-Title-Name-Project-Name. Any XLLN-Module that only targets specific Titles should only activate (return TRUE in BOOL InitXllnModule(...) ) when it is invoked from the correct Title. For example via checksum checks on the Title PE. In the case that access to other XLLN-Modules is required on startup and shutdown then use the XLLNModulePostInit@41101 and XLLNModulePreUninit@41102 exports (with GetModuleHandle, GetProcAddress and the ordinal/export identifier).

#

## XLLN-Online
Provides online play to all LIVE<sup>TM</sup> enabled Titles through an existing hub/master server.
Note that the XLiveLessNess project only simulates online play over LAN (**L**ive **O**ver **L**AN).

## XLLN-Network-Adapter
Overrides XLiveInitialize(...) and XLiveInitializeEx(...) and overrides the preferred network adapter that the Title would usually specify.
The adapter is selected by the user via a MessageBox popup when the Title calls either initialise function.
