#include "../dllmain.hpp"
#include "xlln-module.hpp"
#include "../xlivelessness.hpp"
#include "../utils/utils.hpp"
#include "../utils/util-hook.hpp"
#include "../utils/util-checksum.hpp"
#include <stdint.h>

// #
typedef BOOL(WINAPI *tIsDebuggerPresent)();
DWORD Import_IsDebuggerPresent = 0;
tIsDebuggerPresent DetourIsDebuggerPresent = NULL;
static BOOL WINAPI HookIsDebuggerPresent()
{
	BOOL result = FALSE;
	
	//result = DetourIsDebuggerPresent();
	//MessageBox(NULL, L"Dumber", L"Dumb", 0);
	
	return result;
}

bool string_ends_with(const wchar_t *str, const wchar_t *suffix)
{
	size_t strLen = wcslen(str);
	size_t suffixLen = wcslen(suffix);
	
	bool endsWith =
		strLen >= suffixLen
		&& wcscmp(str + (strLen - suffixLen), suffix) == 0;
	
	return endsWith;
}

// #5028
typedef DWORD(WINAPI *tXLiveLoadLibraryEx)(LPCWSTR lpwszModuleFileName, HINSTANCE *phModule, DWORD dwFlags);
DWORD Import_XLiveLoadLibraryEx = 0;
tXLiveLoadLibraryEx DetourXLiveLoadLibraryEx = NULL;
static DWORD WINAPI HookXLiveLoadLibraryEx(LPCWSTR lpwszModuleFileName, HINSTANCE *phModule, DWORD dwFlags)
{
	DWORD result;
	
	if (string_ends_with(lpwszModuleFileName, L"srsw_shadowrun.dll")) {
		// What this does is load the dll without running dllmain (also consequently without resolving references) in order to hack some of the code in it before it is executed.
		// It is done this way so there is no need for file I/O.
		// Therefore once loaded we need to manually resolve the dll references (import dlls and each of their ordinals).
		// We then overwrite IsDebuggerPresent so that a debugger is reported to be never present while also patching the IsLicensed check to return true.
		// I thought calling LoadLibraryEx again would then execute dllmain without the OS re-resolving references but it does not appear to so we need to then also manually execute it too.
		
		result = DetourXLiveLoadLibraryEx(lpwszModuleFileName, phModule, DONT_RESOLVE_DLL_REFERENCES);
		
		HMODULE hDllSrswShadowrun = GetModuleHandle(lpwszModuleFileName);
		if (!hDllSrswShadowrun) {
			return NULL;
		}
		
		if (PEResolveImports(hDllSrswShadowrun)) {
			return NULL;
		}
		
		uint32_t patchAddress = 0;
		
		{
			PE_HOOK_ARG pe_hack[1];
			DWORD ordinal_addrs[1];
			WORD ordinals[1] = { 569 }; //IsDebuggerPresent
			char dll_name[] = "KERNEL32.dll";
			pe_hack->ordinals_len = 1;
			pe_hack->ordinal_names = NULL;
			pe_hack->ordinal_addrs = ordinal_addrs;
			pe_hack->ordinals = ordinals;
			pe_hack->pe_name = dll_name;
			pe_hack->pe_err = ERROR_FUNCTION_FAILED;
			
			if (PEImportHack(hDllSrswShadowrun, pe_hack, 1)) {
				return NULL;
			}
			
			HookImport(&Import_IsDebuggerPresent, &DetourIsDebuggerPresent, HookIsDebuggerPresent, ordinal_addrs[0]);
		}
		
		{
			// Patch IsLicensed function to always return true.
			patchAddress = (uint32_t)hDllSrswShadowrun + 0x0000383f;
			// MOV AL, 1
			WriteValue<uint16_t>(patchAddress, 0x01B0);
			NopFill(patchAddress + 2, 3);
			
			// Set byte value representing is licensed to 1.
			patchAddress = (uint32_t)hDllSrswShadowrun + 0x001ca588;
			*(uint8_t*)patchAddress = 1;
		}
		
		{
			IMAGE_DOS_HEADER* dos_header = (IMAGE_DOS_HEADER*)hDllSrswShadowrun;
			
			if (dos_header->e_magic != IMAGE_DOS_SIGNATURE) {
				XLLN_DEBUG_LOG(XLLN_LOG_CONTEXT_XLLN_MODULE | XLLN_LOG_LEVEL_FATAL
					, "Not DOS - This file is not a DOS application."
				);
				return ERROR_BAD_EXE_FORMAT;
			}
			
			IMAGE_NT_HEADERS* nt_headers = (IMAGE_NT_HEADERS*)((DWORD)hDllSrswShadowrun + dos_header->e_lfanew);
			
			if (nt_headers->Signature != IMAGE_NT_SIGNATURE) {
				XLLN_DEBUG_LOG(XLLN_LOG_CONTEXT_XLLN_MODULE | XLLN_LOG_LEVEL_FATAL
					, "Not Valid PE - This file is not a valid NT Portable Executable."
				);
				return ERROR_BAD_EXE_FORMAT;
			}
			
			DWORD entryPointAddress = (DWORD)((DWORD)hDllSrswShadowrun + (DWORD)nt_headers->OptionalHeader.AddressOfEntryPoint);
			
			BOOL (APIENTRY *entryPointFunc)(HMODULE hModule, DWORD ul_reason_for_call, LPVOID lpReserved) = (BOOL (APIENTRY *)(HMODULE, DWORD, LPVOID))entryPointAddress;
			
			entryPointFunc(hDllSrswShadowrun, DLL_PROCESS_ATTACH, 0);
		}
		
		{
			// Set byte value representing is licensed to 1.
			patchAddress = (uint32_t)hDllSrswShadowrun + 0x001ca588;
			*(uint8_t*)patchAddress = 1;
		}
		
		return result;
	}
	
	result = DetourXLiveLoadLibraryEx(lpwszModuleFileName, phModule, dwFlags);
	
	return result;
}

typedef void(__stdcall *tLoggingHook)(int, char*, char*);
tLoggingHook pLoggingHook;
static void __stdcall LoggingHook(int a1, char *log_title, char *log_msg)
{
	uint32_t logLevel = XLLN_LOG_CONTEXT_OTHER | XLLN_LOG_LEVEL_DEBUG;
	
	XLLN_DEBUG_LOG(logLevel, "0x%x %s %s", a1, log_title, log_msg);
	
	pLoggingHook(a1, log_title, log_msg);
}

static uint32_t AddPatches()
{
	uint32_t patchAddress = 0;
	DWORD dwBack;
	
	// Improve existing windowed mode.
	{
		patchAddress = (uint32_t)xlln_hmod_title + 0x0000603f + 7;
		WriteValue<uint32_t>(patchAddress, WS_DLGFRAME | WS_BORDER | WS_CAPTION | WS_TABSTOP | WS_GROUP | WS_SIZEBOX | WS_SYSMENU);
		
		//patchAddress = (uint32_t)xlln_hmod_title + 0x002ff01a + 1;
		//WriteValue<uint32_t>(patchAddress, WS_DLGFRAME | WS_BORDER | WS_CAPTION | WS_TABSTOP | WS_GROUP | WS_SIZEBOX | WS_SYSMENU);
		patchAddress = (uint32_t)xlln_hmod_title + 0x002ff048 + 1;
		WriteValue<uint32_t>(patchAddress, WS_DLGFRAME | WS_BORDER | WS_CAPTION | WS_TABSTOP | WS_GROUP | WS_SIZEBOX | WS_SYSMENU);
	}
	
	PE_HOOK_ARG pe_hack[1];
	DWORD ordinal_addrs[1];
	WORD ordinals[1] = { 5028 };
	char dll_name[] = "xlive.dll";
	pe_hack->ordinals_len = 1;
	pe_hack->ordinal_names = NULL;
	pe_hack->ordinal_addrs = ordinal_addrs;
	pe_hack->ordinals = ordinals;
	pe_hack->pe_name = dll_name;
	pe_hack->pe_err = ERROR_FUNCTION_FAILED;
	
	if (PEImportHack(xlln_hmod_title, pe_hack, 1)) {
		return FALSE;
	}
	
	HookImport(&Import_XLiveLoadLibraryEx, &DetourXLiveLoadLibraryEx, HookXLiveLoadLibraryEx, ordinal_addrs[0]);
	
	patchAddress = (uint32_t)xlln_hmod_title + 0x0026e840;
	pLoggingHook = (tLoggingHook)DetourFunc((uint8_t*)patchAddress, (uint8_t*)LoggingHook, 7);
	VirtualProtect(pLoggingHook, 4, PAGE_EXECUTE_READWRITE, &dwBack);
	
	return ERROR_SUCCESS;
}

static uint32_t RemovePatches()
{
	uint32_t patchAddress = 0;
	
	// Undo hooks.
	HookImport(&Import_XLiveLoadLibraryEx, &DetourXLiveLoadLibraryEx, HookXLiveLoadLibraryEx, NULL);
	HookImport(&Import_IsDebuggerPresent, &DetourIsDebuggerPresent, HookIsDebuggerPresent, NULL);
	
	patchAddress = (uint32_t)xlln_hmod_title + 0x0026e840;
	RetourFunc((uint8_t*)patchAddress, (uint8_t*)pLoggingHook, 7);
	free(pLoggingHook);
	
	return ERROR_SUCCESS;
}

// #41101
DWORD WINAPI XLLNModulePostInit()
{
	uint32_t result = ERROR_SUCCESS;
	
	AddPatches();
	
	return result;
}

// #41102
DWORD WINAPI XLLNModulePreUninit()
{
	uint32_t result = ERROR_SUCCESS;
	
	RemovePatches();
	
	return result;
}

BOOL InitXllnModule()
{
	{
		char *checksumTitle = GetPESha256Sum(xlln_hmod_title);
		if (!checksumTitle) {
			return FALSE;
		}
		
		bool titleImageSha256Matches = _strcmpi(checksumTitle, "360a7a55194b52343113dd9a42a89d16ec82c61e0ceb390f569f0cde090e5c52") == 0;
		
		free(checksumTitle);
		
		if (!titleImageSha256Matches) {
			return FALSE;
		}
	}
	
	return TRUE;
}

BOOL UninitXllnModule()
{
	return TRUE;
}
