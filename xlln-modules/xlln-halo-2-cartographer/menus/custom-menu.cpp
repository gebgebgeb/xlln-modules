#include "../../dllmain.hpp"
#include "../xlln-module.hpp"
#include "custom-menu.hpp"
#include "custom-menu-functions.hpp"
#include "../language/custom-labels.hpp"
#include "error/error.hpp"
#include "virtual-keyboard/virtual-keyboard.hpp"
#include "guide/guide.hpp"
#include "credits/credits.hpp"
#include "advanced-settings/advanced-settings.hpp"
#include "advanced-settings/language/language-main.hpp"
#include "advanced-settings/language/language-sub.hpp"
#include "accounts/account-management.hpp"
#include "accounts/account-list.hpp"
#include "accounts/account-create.hpp"
#include "accounts/account-add.hpp"
#include "network-type/network-type.hpp"
#include "advanced-settings/hud-gui/hud-gui.hpp"
#include "advanced-settings/hud-gui/edit-fov.hpp"
#include "advanced-settings/hud-gui/edit-fov-vehicle.hpp"
#include "advanced-settings/hud-gui/edit-crosshair-offset.hpp"
#include "advanced-settings/hud-gui/edit-crosshair-size.hpp"
#include "advanced-settings/toggle-skulls.hpp"

BOOL CMGuideMenu()
{
	if (Title_Version == CLIENT_11122) {
		CustomMenuCall_Guide();
		//Override XLLN GUIDE Menu.
		return TRUE;
	}
	return FALSE;
}

BOOL InitCustomMenu()
{
	if (Title_Version == CLIENT_11122) {
		InitCMFunctions();
		CMSetupVFTables_Error_Inner();
		CMSetupVFTables_Error_Outer();
		CMSetupVFTablesVKb_VKeyTest();
		CMSetupVFTables_Guide();
		CMSetupVFTables_Credits();
		CMSetupVFTables_AdvSettings();
		CMSetupVFTables_LanguageMain();
		CMSetupVFTables_LanguageSub();
		CMSetupVFTables_AccountManagement();
		CMSetupVFTables_AccountList();
		CMSetupVFTables_AccountCreate();
		CMSetupVFTables_AccountAdd();
		CMSetupVFTables_NetworkType();
		CMSetupVFTables_EditHudGui();
		CMSetupVFTables_EditFOV();
		CMSetupVFTables_EditFOVVehicle();
		CMSetupVFTables_EditCrosshairOffset();
		CMSetupVFTables_EditCrosshairSize();
		CMSetupVFTables_ToggleSkulls();
	}

	return TRUE;
}

BOOL UninitCustomMenu()
{
	return TRUE;
}
