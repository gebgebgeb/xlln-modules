#include "../dllmain.hpp"
#include "xlln-module.hpp"
#include "title-patches.hpp"
#include "../utils/util-hook.hpp"
#include "xlive.hpp"
#include "../xlivelessness.hpp"
#include "menus/custom-menu.hpp"
#include "menus/network-type/network-type.hpp"
#include "language/custom-language.hpp"
#include "config/config.hpp"
#include "language/custom-language.hpp"
#include "run-loop/run-loop.hpp"
#include "../utils/utils.hpp"
#define _USE_MATH_DEFINES
#include "math.h"
#include "Tags/TagInterface.hpp"

#pragma region Custom_Language_And_Font

typedef char*(__stdcall *tH2GetLabel)(int, int, int, int);
tH2GetLabel pH2GetLabel;
char* __stdcall H2GetLabel(int a1, int label_id, int a3, int a4)
{ //sub_3defd
	//int label_menu_id = *(int*)(*(int*)a1 + 8 * a3 + 4);
	int label_menu_id = a3;
	char* label = get_cartographer_label(label_menu_id, label_id, 0b10);
	if (label)
		return label;
	label = get_custom_label(current_language, label_menu_id, label_id, 0);
	if (label)
		return label;
	label = get_cartographer_label(label_menu_id, label_id, 0b01);
	if (!label && a1)
		label = pH2GetLabel(a1, label_id, a3, a4);
	//if (strcmp(label, "PROFILE NAME") == 0) {
	//	return label;//in order to breakpoint and get label_id's.
	//}
	if (!H2Config_custom_labels_capture_missing) {
		return label;
	}
	return get_custom_label(current_language, label_menu_id, label_id, label);
}

char* __cdecl cave_c00031b97(char* result, int buff_len)//Font Table Filename Override
{
	strcpy_s(result, buff_len, current_language->font_table_filename);
	return result;
}

char*(__cdecl* pfn_c00031b97)(int, int) = 0;
//__usercall - edi a1, stack a2
__declspec(naked) char* nak_c00031b97()
{
	__asm {
		push ebp
		push edi
		push esi
		push ecx
		push ebx

		mov eax, [esp + 18h]
		push eax//buff_len

		push eax
		call pfn_c00031b97
		add esp, 4h

		push eax//result
		call cave_c00031b97
		add esp, 8h

		pop ebx
		pop ecx
		pop esi
		pop edi
		pop ebp

		retn
	}
}

DWORD langAfterJmpAddr;
__declspec(naked) void getSystemLanguageMethodJmp() {
	CLSetGameLanguage();
	__asm {
		jmp langAfterJmpAddr
	}
}

#pragma endregion

#pragma region Skulls

static uint32_t GetSkullIndexOffset(int skullLabelIndex)
{
	// Skulls are ordered/indexed alphabetically.
	switch (skullLabelIndex) {
	case 0:
		return 0x4D832D; // Anger
	case 1:
		return 0x4D8322; // Assassins
	case 2:
		return 0x4D8328; // Black Eye
	case 3:
		return 0x4D8326; // Blind
	case 4:
		return 0x4D8329; // Catch
	case 5:
		return 0x4D8320; // Envy
	case 6:
		return 0x4D8324; // Famine
	case 7:
		return 0x4D8327; // Ghost
	case 8:
		return 0x4D8321; // Grunt Birthday Party
	case 9:
		return 0x4D832B; // Iron
	case 0xA:
		return 0x4D8325; // IWHBYD
	case 0xB:
		return 0x4D832C; // Mythic
	case 0xC:
		return 0x4D832A; // Sputnik
	case 0xD:
		return 0x4D8323; // Thunderstorm
	case 0xE:
		return 0x4D832E; // Whuppopotamus
	}
	return 0;
}

bool ToggleSkull(int skullLabelIndex)
{
	bool isSkullActive = false;

	if (skullLabelIndex >= 0 && skullLabelIndex < 15) {
		BYTE& skull = *(BYTE*)(GetOffsetAddress(CLIENT_11122, GetSkullIndexOffset(skullLabelIndex)));
		skull = !skull;
		isSkullActive = skull != 0;
	}

	return isSkullActive;
}

bool LoadSkullStatus(int skullLabelIndex)
{
	bool isSkullActive = false;

	if (skullLabelIndex >= 0 && skullLabelIndex < 15) {
		BYTE& skull = *(BYTE*)(GetOffsetAddress(CLIENT_11122, GetSkullIndexOffset(skullLabelIndex)));
		isSkullActive = skull != 0;
	}

	return isSkullActive;
}

#pragma endregion

static bool __cdecl HideIngameChat()
{
	uint32_t GameGlobals = GetOffsetAddress(CLIENT_11122, 0x00482d3c);
	uint32_t* GameEngine = (uint32_t*)(GameGlobals + 0x8);
	uint8_t* GameState = (uint8_t*)GetOffsetAddress(CLIENT_11122, 0x00420fc4);

	if (H2Config_hide_ingame_chat) {
		return true;
	}
	else if (*GameEngine != 3 && *GameState == 3) {
		// Enable chat in engine mode and game state mp.
		return false;
	}
	else {
		// The original test: if is campaign.
		return *GameEngine == 1;
	}
}

// Render first person model.
static uint8_t __cdecl HideFirstPerson(uint32_t a1)
{
	uint8_t result = H2Config_hide_first_person ? 1 : 0;
	//if (AdvLobbySettings_mp_blind & 0b10) {
	//	result = 1;
	//}
	return result;
}

// Render hud.
static uint8_t __cdecl HideHUD(uint32_t a1)
{
	DWORD PlayerGlobalBase = (DWORD)0x30004B60;
	bool& IsPlayerDead = (bool&)*(BYTE*)(PlayerGlobalBase + 4);
	// Fixes game notification text from glitching out.
	if (IsPlayerDead) {
		return 0;
	}
	uint8_t result = H2Config_hide_HUD ? 1 : 0;
	//if (AdvLobbySettings_mp_blind & 0b01) {
	//	result = 1;
	//}
	return result;
}

void SetFOV(int field_of_view_degrees)
{
	static float fov = (float)(70.0f * M_PI / 180.0f);
	static bool fov_redirected = false;
	if (field_of_view_degrees > 0 && field_of_view_degrees <= 110)
	{
		if (!fov_redirected)
		{
			// fld dword ptr[fov]
			BYTE opcode[6] = { 0xD9, 0x05, 0x00, 0x00, 0x00, 0x00 };
			WritePointer((DWORD)&opcode[2], &fov);
			WriteBytes(GetOffsetAddress(CLIENT_11122, 0x000907f3), opcode, sizeof(opcode));

			fov_redirected = true;
		}

		//const double default_radians_field_of_view = 70.0f * M_PI / 180.0f;
		fov = (float)((float)field_of_view_degrees * M_PI / 180.0f);
	}
}

void SetFOVVehicle(int field_of_view_degrees)
{
	if (field_of_view_degrees > 0 && field_of_view_degrees <= 110)
	{
		float calculated_radians_FOV = (float)((float)field_of_view_degrees * M_PI / 180.0f);
		WriteValue(GetOffsetAddress(CLIENT_11122, 0x00413780), calculated_radians_FOV);
	}
}

void SetCrosshairVerticalOffset(float crosshair_offset)
{
	XLLN_DEBUG_LOG(XLLN_LOG_CONTEXT_XLLN_MODULE | XLLN_LOG_LEVEL_INFO
		, "Setting crosshair vertical position."
	);
	if (!FloatIsNaN(crosshair_offset)) {
		XLLN_DEBUG_LOG(XLLN_LOG_CONTEXT_XLLN_MODULE | XLLN_LOG_LEVEL_INFO
			, "Setting crosshair vertical position to: %.6f."
			, crosshair_offset
		);
		tags::tag_data_block* player_controls_block = reinterpret_cast<tags::tag_data_block*>(tags::get_matg_globals_ptr() + 240);
		XLLN_DEBUG_LOG(XLLN_LOG_CONTEXT_XLLN_MODULE | XLLN_LOG_LEVEL_INFO
			, "Player_Controls_Block - Offset: %08x, Count: %d."
			, player_controls_block->block_data_offset
			, player_controls_block->block_count
		);
		if (player_controls_block->block_count > 0)
		{
			for (int i = 0; i < player_controls_block->block_count; i++) {
				XLLN_DEBUG_LOG(XLLN_LOG_CONTEXT_XLLN_MODULE | XLLN_LOG_LEVEL_INFO
					, "Setting Player Control Tag Block Index: %d."
					, player_controls_block->block_count
				);
				*(float*)(tags::get_tag_data() + player_controls_block->block_data_offset + 128 * i + 28) = crosshair_offset;
			}
		}
	}
}

#pragma region Experimental

BOOL __stdcall sub_0000a1d6(int a1)
{
	return a1 == 2 || a1 == 1;
}

BOOL __stdcall sub_0000A276(int a1)
{
	//BOOL(__stdcall* sub_0000a1d6)(int a1) = (BOOL(__stdcall*)(int))GetOffsetAddress(CLIENT_11122, 0x0000a1d6);
	HRESULT(WINAPI* XLivePBufferGetByte)(DWORD *xebBuffer, DWORD dwOffset, BYTE *pucValue) = (HRESULT(WINAPI*)(DWORD*, DWORD, BYTE*))GetOffsetAddress(CLIENT_11122, 0x0000e898);
	DWORD*& var_c00479e78 = *(DWORD**)(GetOffsetAddress(CLIENT_11122, 0x00479e78));

	bool v4 = sub_0000a1d6(a1) == 0;

	return a1 != 1 || OnlineModeFunctional;

	BYTE a5 = 0;
	HRESULT rawr = XLivePBufferGetByte(var_c00479e78, 0, &a5);
	return !a5 || v4;
}

void __stdcall sub_1F98258(int a1, int a2)
{
	int(__thiscall* sub_EA1973)(DWORD *thisptr, WORD a2) = (int(__thiscall*)(DWORD*, WORD))GetOffsetAddress(CLIENT_11122, 0x00211973);
	//bool(__stdcall* sub_0000A276)(int a1) = (bool(__stdcall*)(int))GetOffsetAddress(CLIENT_11122, 0x0000A276);
	void(__thiscall* sub_EABF85)(void *thisptr, int a2) = (void(__thiscall*)(void*, int))GetOffsetAddress(CLIENT_11122, 0x0021BF85);

	__int16 v2; // bp
	int v3; // esi
	char v4; // al
	char v5; // bl
	BYTE *v6; // ebp

	v2 = *(WORD*)(a1 + 112);
	*(BYTE*)(sub_EA1973((DWORD*)a1, 0) + 110) = 0;
	v3 = v2;
	v5 = v4 = sub_0000A276(v2);
	//v5 = v4 = true;
	v6 = (BYTE*)sub_EA1973((DWORD*)a1, v4 == 0);
	*(BYTE*)(a1 + 120) = v5;
	if (v6)
	{
		switch (v3)
		{
		case 0:
			sub_EABF85(v6, 0x800010B);
			v6[110] = 1;
			break;
		case 1:
			sub_EABF85(v6, 0x9000284);
			v6[110] = 1;
			break;
		case 2:
			sub_EABF85(v6, 0xA000285);
			v6[110] = 1;
			break;
		case 3:
			sub_EABF85(v6, 0x8000286);
			v6[110] = 1;
			break;
		case 4:
			sub_EABF85(v6, 0x5000986);
			v6[110] = 1;
			break;
		case 5:
			sub_EABF85(v6, 0x40002ED);
			v6[110] = 1;
			break;
		default:
			sub_EABF85(v6, -1);
			v6[110] = 1;
			break;
		}
	}
}

int __stdcall sub_15FD028(int a1, DWORD *a2)
{
	//bool(__stdcall* sub_0000A276)(int a1) = (bool(__stdcall*)(int))GetOffsetAddress(CLIENT_11122, 0x0000A276);
	int(__cdecl* sub_CC23EA)(int a1) = (int(__cdecl*)(int))GetOffsetAddress(CLIENT_11122, 0x000323EA);
	BOOL(__cdecl* sub_C94544)(int a1) = (BOOL(__cdecl*)(int))GetOffsetAddress(CLIENT_11122, 0x00004544);

	int(__stdcall* sub_C9B198)(int a1) = (int(__stdcall*)(int))GetOffsetAddress(CLIENT_11122, 0x0000b198);
	int(__stdcall* sub_C9B257)(int a1) = (int(__stdcall*)(int))GetOffsetAddress(CLIENT_11122, 0x0000b257);
	int(__stdcall* sub_C9A978)(int a1) = (int(__stdcall*)(int))GetOffsetAddress(CLIENT_11122, 0x0000a978);
	int(__stdcall* sub_C9B32B)(int a1) = (int(__stdcall*)(int))GetOffsetAddress(CLIENT_11122, 0x0000B32B);
	DWORD(WINAPI* XShowGuideUI)(DWORD dwUserIndex) = (DWORD(WINAPI*)(DWORD))GetOffsetAddress(CLIENT_11122, 0x0000e8a4);
	int(__stdcall* sub_C9A307)(int a1) = (int(__stdcall*)(int))GetOffsetAddress(CLIENT_11122, 0x0000A307);

	int v2; // esi
	int result; // eax

	v2 = *a2 & 0xFFFF;
	result = sub_0000A276(v2);
	if ((BYTE)result)
	{
		// I don't think result gets altered from here on?
		// Honestly IDK if this function returns int, bool or void.
		// if void just integrate the if above and delete all the writes to result.
		// and change the return to all those (a1) funcs below to void probs.
		if (v2 != 5)
			sub_CC23EA(v2);
		sub_C94544(20);
		result = sub_C94544(19);
		switch (v2)
		{
		case 0:
			result = sub_C9B198(a1);
			break;
		case 1:
			CustomMenuCall_NetworkType(true);
			//result = sub_C9B257(0);
			break;
		case 2:
			CustomMenuCall_NetworkType(false);
			//result = sub_C9A978(a1);
			break;
		case 3:
			result = sub_C9B32B(a1);
			break;
		case 4:
			result = XShowGuideUI(0);
			break;
		case 5:
			result = sub_C9A307(a1);
			break;
		default:
			return result;
		}
		return 0;
	}
	return result;
}

static int __fastcall sub_250DD8(void *thisptr, DWORD _EDX, int a2, DWORD *a3)//__thiscall
{
	signed int(__cdecl* sub_00263d0e)(signed int* a1) = (signed int(__cdecl*)(signed int*))GetOffsetAddress(CLIENT_11122, 0x00263d0e);
	int(__cdecl* sub_002096da)(int a1, int a2) = (int(__cdecl*)(int, int))GetOffsetAddress(CLIENT_11122, 0x002096da);

	void *v3; // esi
	int v4; // ST04_4
	int v5; // eax

	v3 = thisptr;
	a3 = (DWORD *)(*a3 & 0xFFFF);
	sub_00263d0e((signed int *)&a3);
	v4 = 4;// (*(int(__thiscall **)(void *))(*(DWORD *)v3 + 56))(v3);
	v5 = 3;// (*(int(__thiscall **)(void *))(*(DWORD *)v3 + 52))(v3);
	return sub_002096da(v5, v4);
}

typedef int(__cdecl *tfn_0020B8C3)(DWORD*, DWORD*);
tfn_0020B8C3 pfn_0020B8C3;
static int __cdecl fn_0020B8C3(DWORD *a1, DWORD *a2)
{
	char menu_details[150];
	snprintf(menu_details, 150, "MENU l0:%hX h0:%hX 1:%X 2:%X 3:%X 4:%X 5:%X 6:%X 7:%X", ((WORD*)a2)[0], ((WORD*)a2)[1], a2[1], a2[2], a2[3], a2[4], a2[5], a2[6], a2[7]);
	XLLNDebugLog(0, menu_details);
	int result = pfn_0020B8C3(a1, a2);
	return result;
}

static int __fastcall widget_button_handler_matchmaking(void *thisptr, DWORD _EDX, int a2, DWORD *a3)
{
	int(__thiscall* sub_00212604)(unsigned __int16 *a1, int) = (int(__thiscall*)(unsigned __int16 *, int))GetOffsetAddress(CLIENT_11122, 0x00212604);
	/*WORD*(__thiscall* sub_00211ba1)(DWORD *a1) = (WORD*(__thiscall*)(DWORD*))GetOffsetAddress(CLIENT_11122, 0x00211ba1);
	//unsigned __int16 *dumb = sub_00211ba1((DWORD*)thisptr);
	int result = sub_00212604((unsigned __int16 *)thisptr, 3);
	return result;*/

	unsigned __int16 *v3; // esi
	int(__cdecl *v4)(DWORD); // eax
	int result; // eax

	v3 = (unsigned __int16 *)thisptr;
	v4 = (int(__cdecl *)(DWORD))((DWORD*)thisptr)[1760];
	if (!v4 || (result = v4(*(DWORD *)(*(DWORD *)a2 + 4)), (BYTE)result))
		result = sub_00212604(v3, 3);
	return result;
}

#pragma endregion

typedef bool(__cdecl *tOnMapLoad)(DWORD*);
tOnMapLoad pOnMapLoad;
static bool __cdecl OnMapLoad(DWORD *a1)
{
	bool resultLoadSuccess = pOnMapLoad(a1);
	if (resultLoadSuccess) {
		SetCrosshairVerticalOffset(H2Config_crosshair_offset);
	}
	return resultLoadSuccess;
}

bool __cdecl XllnGuideUiHandler()
{
	if (Title_Version == CLIENT_11081 || Title_Version == CLIENT_11091 || Title_Version == CLIENT_11122) {
		// #5215
		typedef DWORD(WINAPI *tXShowGuideUI)(DWORD dwUserIndex);
		tXShowGuideUI AnyXShowGuideUI = (tXShowGuideUI)**(DWORD**)(GetOffsetAddress(0x0000e8a4 + 2, 0x0000e8a4 + 2, 0x0000e8a4 + 2));
		AnyXShowGuideUI(0);
		
		return true;
	}
	return false;
}

BOOL InitTitlePatches()
{
	if (Title_Version == CLIENT_11122) {
		DWORD dwBack;

		// Useful for finding/logging different kinds of menus that get rendered.
		//pfn_0020B8C3 = (tfn_0020B8C3)DetourFunc((BYTE*)GetOffsetAddress(CLIENT_11122, 0x0020B8C3), (BYTE*)fn_0020B8C3, 7);
		//VirtualProtect(pfn_0020B8C3, 4, PAGE_EXECUTE_READWRITE, &dwBack);

		// I forgot what this is.
		//WritePointer(GetOffsetAddress(CLIENT_11122, 0x0020dd5d), widget_button_handler_matchmaking);

		// Brightness Button Click Handler.
		//WritePointer(GetOffsetAddress(CLIENT_11122, 0x00250ea7), sub_250DD8);
		
		// Mainmenu button press handler.
		//WritePointer(GetOffsetAddress(CLIENT_11122, 0x0000b626), sub_15FD028);
		// Mainmenu button enabled handler.
		//WritePointer(GetOffsetAddress(CLIENT_11122, 0x0039c09c), sub_1F98258);

		// Licence check before calling XLivePBufferGetByte.
		//PatchCall(GetOffsetAddress(CLIENT_11122, 0x0000a276), sub_0000A276);

		// Required for Custom Menus and Custom Language.
		pH2GetLabel = (tH2GetLabel)DetourClassFunc((BYTE*)GetOffsetAddress(CLIENT_11122, 0x0003defd), (BYTE*)H2GetLabel, 8);
		VirtualProtect(pH2GetLabel, 4, PAGE_EXECUTE_READWRITE, &dwBack);

		// Hook the function that sets the font table filename.
		pfn_c00031b97 = (char*(__cdecl*)(int, int))(GetOffsetAddress(CLIENT_11122, 0x00031b97));
		PatchCall(GetOffsetAddress(CLIENT_11122, 0x00031e89), nak_c00031b97);

		// Hook the part where it sets the global language id.
		langAfterJmpAddr = (DWORD)(GetOffsetAddress(CLIENT_11122, 0x0003828c));
		DetourFunc((BYTE*)GetOffsetAddress(CLIENT_11122, 0x0003820d), (BYTE*)getSystemLanguageMethodJmp, 6);

		// Redirects the is_campaign call that the in-game chat renderer makes so we can show/hide it as we like.
		PatchCall(GetOffsetAddress(CLIENT_11122, 0x0022667b), HideIngameChat);
		PatchCall(GetOffsetAddress(CLIENT_11122, 0x00226628), HideIngameChat);

		PatchCall(GetOffsetAddress(CLIENT_11122, 0x00228579), HideFirstPerson);
		PatchCall(GetOffsetAddress(CLIENT_11122, 0x00223955), HideHUD);

		// SERVER_11122 0x00001f35c
		pOnMapLoad = (tOnMapLoad)DetourFunc((BYTE*)GetOffsetAddress(CLIENT_11122, 0x00008f62), (BYTE*)OnMapLoad, 11);
		VirtualProtect(pOnMapLoad, 4, PAGE_EXECUTE_READWRITE, &dwBack);
	}

	InitCustomLanguage();
	InitCustomMenu();
	InitRunLoop();
	
	uint32_t setHandler = (uint32_t)XllnGuideUiHandler;
	uint32_t resultXllnModifyProperty = XLLNModifyProperty(XLLNModifyPropertyTypes::tGUIDE_UI_HANDLER, &setHandler, 0);
	
	// Initialise any set values.
	if (Title_Version == CLIENT_11122) {
		SetFOV(H2Config_field_of_view);
		SetFOVVehicle(H2Config_field_of_view_vehicle);

		XLLN_DEBUG_LOG(XLLN_LOG_CONTEXT_XLLN_MODULE | XLLN_LOG_LEVEL_INFO
			, "Patching Motion Sensor fix."
		);
		WriteValue<BYTE>(GetOffsetAddress(CLIENT_11122, 0x002849c4), (BYTE)4);
	}

	return TRUE;
}

BOOL UninitTitlePatches()
{
	uint32_t setHandler = (uint32_t)0;
	uint32_t resultXllnModifyProperty = XLLNModifyProperty(XLLNModifyPropertyTypes::tGUIDE_UI_HANDLER, &setHandler, 0);
	
	UninitRunLoop();
	UninitCustomMenu();
	UninitCustomLanguage();
	return TRUE;
}
