#pragma once
#include <stdint.h>

extern bool H2Portable;
extern int H2Config_language_code_main;
extern int H2Config_language_code_variant;
extern bool H2Config_custom_labels_capture_missing;
extern int H2Config_hotkeyIdGuide;
extern bool H2Config_hide_ingame_chat;
extern bool H2Config_hide_first_person;
extern bool H2Config_hide_HUD;
extern int32_t H2Config_field_of_view;
extern int32_t H2Config_field_of_view_vehicle;
extern float H2Config_crosshair_offset;

HRESULT InitConfig();
HRESULT UninitConfig();
