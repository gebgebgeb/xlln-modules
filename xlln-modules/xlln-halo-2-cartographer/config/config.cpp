#include <Winsock2.h>
#include "../../dllmain.hpp"
#include "config.hpp"
#include "../../utils/utils.hpp"
#include "../xlln-module-resource.h"
#include "../../xlivelessness.hpp"
#include <stdio.h>
#include <vector>

bool H2Portable = false;
int H2Config_language_code_main = -1;
int H2Config_language_code_variant = 0;
bool H2Config_custom_labels_capture_missing = false;
int H2Config_hotkeyIdGuide = VK_HOME;
bool H2Config_hide_ingame_chat = false;
bool H2Config_hide_first_person = false;
bool H2Config_hide_HUD = false;
int32_t H2Config_field_of_view = 70;
int32_t H2Config_field_of_view_vehicle = 70;
float H2Config_crosshair_offset = NAN;

const char XllnConfigHeader[] = "Halo-2-Project-Cartographer-Config-Version";
const char XllnConfigVersion[] = DLL_VERSION_STR;

typedef struct {
	bool keepInvalidLines = false;
	bool saveValuesRead = true;
	std::vector<char*> badConfigEntries;
	std::vector<char*> otherConfigHeaderEntries;
	std::vector<char*> readSettings;
} INTERPRET_CONFIG_CONTEXT;

static int interpretConfigSetting(const char *fileLine, const char *version, size_t lineNumber, void *interpretationContext)
{
	if (!version) {
		return 0;
	}
	INTERPRET_CONFIG_CONTEXT &configContext = *(INTERPRET_CONFIG_CONTEXT*)interpretationContext;

	bool unrecognised = false;
	bool duplicated = false;
	bool incorrect = false;
	size_t fileLineLen = strlen(fileLine);

	if (fileLine[0] == '#' || fileLine[0] == ';' || fileLineLen <= 2) {
		unrecognised = true;
	}
	// When the version of the header has changed.
	else if (version[-1] != 0) {
		unrecognised = true;
	}
	else {
		const char *equals = strchr(fileLine, '=');

		if (!equals || equals == fileLine) {
			unrecognised = true;
		}
		else {
			size_t iValueSearch = 0;
			const char *value = equals + 1;
			while (*value == '\t' || *value == ' ') {
				value++;
			}

			const char *whitespace = equals;
			while (&whitespace[-1] != fileLine && (whitespace[-1] == '\t' || whitespace[-1] == ' ')) {
				whitespace--;
			}
			size_t settingStrLen = whitespace - fileLine;

			char *settingName = new char[settingStrLen + 1];
			memcpy(settingName, fileLine, settingStrLen);
			settingName[settingStrLen] = 0;

			for (char *&readSettingName : configContext.readSettings) {
				if (_stricmp(readSettingName, settingName) == 0) {
					duplicated = true;
					break;
				}
			}

#define SettingNameMatches(x) _stricmp(x, settingName) == 0

			if (!unrecognised && !duplicated && !incorrect) {

				if (SettingNameMatches("hide_ingame_chat")) {
					uint32_t tempuint32;
					if (sscanf_s(value, "%u", &tempuint32) == 1) {
						if (configContext.saveValuesRead) {
							H2Config_hide_ingame_chat = tempuint32 > 0;
						}
					}
					else {
						incorrect = true;
					}
				}
				else if (SettingNameMatches("hide_first_person")) {
					uint32_t tempuint32;
					if (sscanf_s(value, "%u", &tempuint32) == 1) {
						if (configContext.saveValuesRead) {
							H2Config_hide_first_person = tempuint32 > 0;
						}
					}
					else {
						incorrect = true;
					}
				}
				else if (SettingNameMatches("hide_HUD")) {
					uint32_t tempuint32;
					if (sscanf_s(value, "%u", &tempuint32) == 1) {
						if (configContext.saveValuesRead) {
							H2Config_hide_HUD = tempuint32 > 0;
						}
					}
					else {
						incorrect = true;
					}
				}
				else if (SettingNameMatches("field_of_view")) {
					int32_t tempint32;
					if (sscanf_s(value, "%d", &tempint32) == 1) {
						if (configContext.saveValuesRead) {
							H2Config_field_of_view = tempint32;
						}
					}
					else {
						incorrect = true;
					}
				}
				else if (SettingNameMatches("field_of_view_vehicle")) {
					int32_t tempint32;
					if (sscanf_s(value, "%d", &tempint32) == 1) {
						if (configContext.saveValuesRead) {
							H2Config_field_of_view_vehicle = tempint32;
						}
					}
					else {
						incorrect = true;
					}
				}
				else if (SettingNameMatches("crosshair_offset")) {
					float tempfloat;
					if (sscanf_s(value, "%f", &tempfloat) == 1 && (FloatIsNaN(tempfloat) || (tempfloat >= 0.0f && tempfloat <= 0.53f))) {
						if (configContext.saveValuesRead) {
							H2Config_crosshair_offset = tempfloat;
						}
					}
					else {
						incorrect = true;
					}
				}
				else {
					unrecognised = true;
				}

				if (!unrecognised && !duplicated && !incorrect) {
					configContext.readSettings.push_back(CloneString(settingName));
				}
			}

			delete[] settingName;
		}
	}

	if (unrecognised || duplicated || incorrect) {

		if (configContext.keepInvalidLines) {
			// When the version of the header has changed.
			if (version[-1] != 0) {
				configContext.otherConfigHeaderEntries.push_back(CloneString(fileLine));
			}
			else {
				configContext.badConfigEntries.push_back(CloneString(fileLine));
			}
		}
	}
	return 0;
}

HRESULT SaveXllnConfig(const wchar_t *file_config_path, INTERPRET_CONFIG_CONTEXT *configContext)
{
	FILE* fileConfig = 0;

	errno_t err = _wfopen_s(&fileConfig, file_config_path, L"wb");
	if (err) {
		return E_HANDLE;
	}

#define WriteText(text) fputs(text, fileConfig)
#define WriteTextF(format, ...) fprintf_s(fileConfig, format, __VA_ARGS__)

	WriteText("#--- Halo 2 Project Cartographer Configuration File ---");
	WriteText("\n");

	WriteText("\n# hide_ingame_chat:");
	WriteText("\n# Valid values:");
	WriteText("\n#   0 - (DEFAULT) Allows the ingame chat to be visible.");
	WriteText("\n#   1 - Never show the ingame chat.");
	WriteText("\n# Allows permanently hiding the ingame chat in games.");
	WriteText("\n");

	WriteText("\n# hide_first_person:");
	WriteText("\n# Valid values:");
	WriteText("\n#   0 - (DEFAULT) Shows the first person model.");
	WriteText("\n#   1 - Hides the first person model.");
	WriteText("\n# Adds the ability to hide and show the first person model (arms and gun) of the player. This is like the Blind skull but it does not affect the HUD.");
	WriteText("\n");

	WriteText("\n# hide_HUD:");
	WriteText("\n# Valid values:");
	WriteText("\n#   0 - (DEFAULT) Shows the HUD.");
	WriteText("\n#   1 - Hides the HUD.");
	WriteText("\n# Adds the ability to hide the Heads Up Display (HUD). This is like the Blind skull but it does not affect the first person model (arms and gun).");
	WriteText("\n");

	WriteText("\n# field_of_view:");
	WriteText("\n# Valid values: 1 to 110.");
	WriteText("\n#   70 - (DEFAULT)");
	WriteText("\n# Adds the ability to change the Field of View.");
	WriteText("\n");

	WriteText("\n# field_of_view_vehicle:");
	WriteText("\n# Valid values: 1 to 110.");
	WriteText("\n#   70 - (DEFAULT)");
	WriteText("\n# Adds the ability to change the Field of View when the player is in a vehicle. Node this setting does not affect all third person view modes.");
	WriteText("\n");

	WriteText("\n# crosshair_offset:");
	WriteText("\n# Valid values: NaN, 0 to 0.53.");
	WriteText("\n#   0.165 - (DEFAULT)");
	WriteText("\n#   NaN   - Disables the setting.");
	WriteText("\n# Adds the ability to adjust the height on screen of the crosshair / gun reticles where 0 is the center of the screen and increasing in value is lower on-screen.");
	WriteText("\n");

	WriteText("\n");
	WriteTextF("\n[%s:%s]", XllnConfigHeader, XllnConfigVersion);
	WriteTextF("\nhide_ingame_chat = %u", H2Config_hide_ingame_chat ? 1 : 0);
	WriteTextF("\nhide_first_person = %u", H2Config_hide_first_person ? 1 : 0);
	WriteTextF("\nhide_HUD = %u", H2Config_hide_HUD ? 1 : 0);
	WriteTextF("\nfield_of_view = %d", H2Config_field_of_view);
	WriteTextF("\nfield_of_view_vehicle = %d", H2Config_field_of_view_vehicle);
	if (FloatIsNaN(H2Config_crosshair_offset)) {
		WriteText("\ncrosshair_offset = NaN");
	}
	else {
		WriteTextF("\ncrosshair_offset = %f", H2Config_crosshair_offset);
	}
	WriteText("\n\n");

	if (configContext) {
		for (char *&badEntry : configContext->badConfigEntries) {
			WriteText(badEntry);
			WriteText("\n");
		}
		if (configContext->badConfigEntries.size()) {
			WriteText("\n\n");
		}
		for (char *&otherEntry : configContext->otherConfigHeaderEntries) {
			WriteText(otherEntry);
			WriteText("\n");
		}
		if (configContext->otherConfigHeaderEntries.size()) {
			WriteText("\n");
		}
	}

	fclose(fileConfig);

	return S_OK;
}

static uint32_t XllnConfig(const wchar_t *config_filepath, bool save_config)
{
	// Always read the/a config file before saving.
	uint32_t result = ERROR_FUNCTION_FAILED;
	FILE* fileConfig = 0;
	errno_t errorFopen = _wfopen_s(&fileConfig, config_filepath, L"rb");
	if (!fileConfig) {
		if (errorFopen == ENOENT) {
			result = ERROR_FILE_NOT_FOUND;
			XLLN_DEBUG_LOG(XLLN_LOG_CONTEXT_XLIVELESSNESS | XLLN_LOG_LEVEL_WARN, "Config file not found: \"%ls\".", config_filepath);
		}
		else {
			XLLN_DEBUG_LOG(XLLN_LOG_CONTEXT_XLIVELESSNESS | XLLN_LOG_LEVEL_WARN, "Config file read error: %d, \"%ls\".", errorFopen, config_filepath);
			return ERROR_FUNCTION_FAILED;
		}
	}

	INTERPRET_CONFIG_CONTEXT *interpretationContext = 0;
	if (fileConfig) {
		interpretationContext = new INTERPRET_CONFIG_CONTEXT;
		interpretationContext->keepInvalidLines = true;
		interpretationContext->saveValuesRead = !save_config;

		ReadIniFile(fileConfig, true, XllnConfigHeader, XllnConfigVersion, interpretConfigSetting, (void*)interpretationContext);

		for (char *&readSettingName : interpretationContext->readSettings) {
			delete[] readSettingName;
		}
		interpretationContext->readSettings.clear();

		fclose(fileConfig);
		fileConfig = 0;
	}

	wchar_t *saveToConfigPath = PathFromFilename(config_filepath);
	uint32_t errorMkdir = EnsureDirectoryExists(saveToConfigPath);
	delete[] saveToConfigPath;
	if (errorMkdir) {
		result = ERROR_DIRECTORY;
		XLLN_DEBUG_LOG(XLLN_LOG_CONTEXT_XLIVELESSNESS | XLLN_LOG_LEVEL_WARN, "EnsureDirectoryExists(...) error: %u, \"%ls\".", errorMkdir, config_filepath);
	}
	else {
		uint32_t errorSaveConfig = SaveXllnConfig(config_filepath, interpretationContext);
		if (errorSaveConfig) {
			result = errorSaveConfig;
			XLLN_DEBUG_LOG(XLLN_LOG_CONTEXT_XLIVELESSNESS | XLLN_LOG_LEVEL_WARN, "SaveXllnConfig(...) error: %u, \"%ls\".", errorSaveConfig, config_filepath);
		}
		else {
			result = ERROR_SUCCESS;
		}
	}

	if (interpretationContext) {
		for (char *&badEntry : interpretationContext->badConfigEntries) {
			delete[] badEntry;
		}
		interpretationContext->badConfigEntries.clear();
		for (char *&otherEntry : interpretationContext->otherConfigHeaderEntries) {
			delete[] otherEntry;
		}
		interpretationContext->otherConfigHeaderEntries.clear();
	}

	return result;
}

static wchar_t *pcarto_config = 0;

HRESULT InitConfig()
{
	size_t storagePathBufSize = 0;
	uint32_t errorGetStoragePath = XLLNGetXLLNStoragePath((uint32_t)xlln_hmod_xlln_module, 0, 0, &storagePathBufSize);
	if (errorGetStoragePath == ERROR_INSUFFICIENT_BUFFER) {
		uint32_t xllnLocalInstanceId = 0;
		wchar_t *storagePath = new wchar_t[storagePathBufSize / sizeof(wchar_t)];
		errorGetStoragePath = XLLNGetXLLNStoragePath((uint32_t)xlln_hmod_xlln_module, &xllnLocalInstanceId, storagePath, &storagePathBufSize);
		if (errorGetStoragePath == ERROR_SUCCESS) {
			pcarto_config = FormMallocString(L"%sxlln-halo-2-cartographer-%u.ini", storagePath, xllnLocalInstanceId);
			XllnConfig(pcarto_config, false);
		}
		delete[] storagePath;
	}
	return ERROR_SUCCESS;
}

HRESULT UninitConfig()
{
	if (pcarto_config) {
		XllnConfig(pcarto_config, true);
		free(pcarto_config);
		pcarto_config = 0;
	}
	return ERROR_SUCCESS;
}
