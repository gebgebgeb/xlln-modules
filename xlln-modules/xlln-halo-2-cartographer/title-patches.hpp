#pragma once

BOOL InitTitlePatches();
BOOL UninitTitlePatches();

char* __stdcall H2GetLabel(int a1, int label_id, int a3, int a4);
bool ToggleSkull(int skullLabelIndex);
bool LoadSkullStatus(int skullLabelIndex);
void SetFOV(int field_of_view_degrees);
void SetFOVVehicle(int field_of_view_degrees);
void SetCrosshairVerticalOffset(float crosshair_offset);
